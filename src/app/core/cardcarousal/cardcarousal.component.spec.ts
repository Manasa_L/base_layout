import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardcarousalComponent } from './cardcarousal.component';

describe('CardcarousalComponent', () => {
  let component: CardcarousalComponent;
  let fixture: ComponentFixture<CardcarousalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardcarousalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardcarousalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
