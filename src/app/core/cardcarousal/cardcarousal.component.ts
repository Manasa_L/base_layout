import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cardcarousal',
  templateUrl: './cardcarousal.component.html',
  styleUrls: ['./cardcarousal.component.css']
})
export class CardcarousalComponent implements OnInit {
  @ViewChild("carousal") carousal  : ElementRef;
  left;
  right;
  width;
  scrollwidth;
  constructor() {
    
   }

   ngOnInit(){

   }

  ngAfterViewInit() {
    console.log(this.carousal, this.carousal.nativeElement, this.carousal.nativeElement.scrollWidth);
    this.left = this.carousal.nativeElement.scrollLeft;
    this.width = this.carousal.nativeElement.offsetWidth
    this.right = this.left + this.width;
    this.scrollwidth = this.carousal.nativeElement.scrollWidth;
    console.log(this.left,this.right,this.width);
    
  }

  move(dir){
    if(dir == 'left'){
      if(this.left - this.width > 0){
        this.left = this.left - this.width;
        this.right = this.right - this.width;
      } else {
        this.left = 0;
        this.right = this.width
      }
    } else {
      if(this.right + this.width < this.scrollwidth){        
        this.right = this.right + this.width;
        this.left = this.left + this.width;
      } else {
        this.right = this.scrollwidth;
        this.left = this.scrollwidth - this.width;
      }
    }
    this.carousal.nativeElement.scrollTo({
      left : this.left,
      behavior : 'smooth'
    });
  }

}
