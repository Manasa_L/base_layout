import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cards = ['one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen']
  constructor() { }

  ngOnInit() {
  }

}
