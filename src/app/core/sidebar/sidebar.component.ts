import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import {CoreService} from '../core.service';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { filter, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  navigation;
  sideBarItems;
  hoveredItem = null;
  showSubPanel = false;
  // @ViewChild("subsidebar") subsidebar : ElementRef;
  // @HostListener('document:click',['$event'])
  // clickout(event){
  //   if(this.showSubPanel){
  //     if(this.subsidebar.nativeElement.contains(event.target)){

  //     }else{
  //       this.showSubPanel = false
  //     }
  //   }
  // }
  constructor(service : CoreService, private route : ActivatedRoute, private router : Router) {
    this.sideBarItems = service.getSideNavItems();
    console.log(this.router);    
    this.router.events.pipe(
      filter((event) => event instanceof NavigationStart),
      debounceTime(400)
    ).subscribe(
      x => {
      console.log('val',x); /*Redirect to Home*/
}
)
   }

   enter(item){
     if(this.hoveredItem && item.name == this.hoveredItem.name){
       this.showSubPanel=false;
       this.hoveredItem=null;
     } else {
       this.hoveredItem= item
       this.showSubPanel=true
     }
   }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.navigation = routeParams.type
      console.log(this.navigation,routeParams,"changed");     
    })
  }

}
