import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  constructor() { }

  getSideNavItems(){
    return [
      {
        name : "Power Me",
        icon : '/assests/blah.png',
        route : '/powerme'
      },
      {
        name : "Dashboard",
        icon : '/assests/blah.png',
        route : '/dashboard'
      },
      {
        name : "Analysis",
        icon : '/assests/blah.png',
        route : '/analysis'
      },
      {
        name : "Rationalise",
        icon : '/assests/blah.png',
        route : '/rationalise'
      },
      {
        name : "BI Systems",
        icon : '/assests/blah.png',
        children : [
          {
            name : "OBIEE",
            icon : './assests.blah.png',
            route : '/bi-catalog/obiee'
          },
          {
            name : "Micro Strategy",
            icon : './assests.blah.png',
            route : '/bi-catalog/microstrategy'
          },
          {
            name : "Tableau",
            icon : './assests.blah.png',
            route : '/bi-catalog/tableau'
          },
          {
            name : "Power BI",
            icon : './assests.blah.png',
            route : '/bi-catalog/powerbi'
          }
        ]
      },
      {
        name : "Data Systems",
        icon : '/assests/blah.png',
        children : [
          {
            name : "Hive",
            icon : './assests.blah.png',
            route : '/data-catalog/hive'
          },
          {
            name : "Redshift",
            icon : './assests.blah.png',
            route : '/data-catalog/redshift'
          }
        ]
      }
    ]
  }
}
