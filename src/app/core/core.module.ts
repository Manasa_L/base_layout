import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {RouterModule} from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CardcarousalComponent } from './cardcarousal/cardcarousal.component';

@NgModule({
  declarations: [HomeComponent, SidebarComponent, CardcarousalComponent],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class CoreModule { }
