import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DatasystemRoutingModule } from './datasystem-routing.module';
import { DatasystemComponent } from './datasystem/datasystem.component';

@NgModule({
  declarations: [DatasystemComponent],
  imports: [
    CommonModule,
    DatasystemRoutingModule
  ]
})
export class DatasystemModule { }
