import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DatasystemComponent} from './datasystem/datasystem.component';

const routes: Routes = [
  {
    path: '',
    component : DatasystemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatasystemRoutingModule { }
