import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasystemComponent } from './datasystem.component';

describe('DatasystemComponent', () => {
  let component: DatasystemComponent;
  let fixture: ComponentFixture<DatasystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
