import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-datasystem',
  templateUrl: './datasystem.component.html',
  styleUrls: ['./datasystem.component.css']
})
export class DatasystemComponent implements OnInit {
  type;
  constructor(private route : ActivatedRoute) { 
    
  }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.type = routeParams.type
    })
  }

}
