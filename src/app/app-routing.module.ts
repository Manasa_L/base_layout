import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './core/home/home.component';
import {SidebarComponent} from './core/sidebar/sidebar.component';

const routes: Routes = [
  {
    path : '',
    component : HomeComponent,
    children : [
      {
        path : 'dashboard',
        loadChildren : './dashboard/dashboard.module#DashboardModule'
      },
      {
        path : 'rationalise',
        loadChildren : './rationalise/rationalise.module#RationaliseModule'
      },
      {
        path : 'analysis',
        loadChildren : './analysis/analysis.module#AnalysisModule'
      },
      {
        path : 'bi-catalog/:type',
        loadChildren : './bisystem/bisystem.module#BisystemModule'
      },
      {
        path : 'data-catalog/:type',
        loadChildren : './datasystem/datasystem.module#DatasystemModule'
      },
      {
        path : '',
        outlet : 'sidebar',
        component : SidebarComponent
      },
      {
        path : 'something',
        outlet : 'sidebar',
        component : SidebarComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
