import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BisystemRoutingModule } from './bisystem-routing.module';
import { BiComponent } from './bi/bi.component';

@NgModule({
  declarations: [BiComponent],
  imports: [
    CommonModule,
    BisystemRoutingModule
  ]
})
export class BisystemModule { }
