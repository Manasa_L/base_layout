import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bi',
  templateUrl: './bi.component.html',
  styleUrls: ['./bi.component.css']
})
export class BiComponent implements OnInit {
  type;
  constructor(private route : ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.type = routeParams.type
    })
  }

}
