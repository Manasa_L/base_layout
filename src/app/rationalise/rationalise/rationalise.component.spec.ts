import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RationaliseComponent } from './rationalise.component';

describe('RationaliseComponent', () => {
  let component: RationaliseComponent;
  let fixture: ComponentFixture<RationaliseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RationaliseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RationaliseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
