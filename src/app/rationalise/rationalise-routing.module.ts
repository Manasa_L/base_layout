import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RationaliseComponent } from './rationalise/rationalise.component';

const routes: Routes = [
  {
    path:'',
    component: RationaliseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RationaliseRoutingModule { }
