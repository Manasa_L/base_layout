import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RationaliseRoutingModule } from './rationalise-routing.module';
import { RationaliseComponent } from './rationalise/rationalise.component';

@NgModule({
  declarations: [RationaliseComponent],
  imports: [
    CommonModule,
    RationaliseRoutingModule
  ]
})
export class RationaliseModule { }
